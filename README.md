![Alt text](https://img.shields.io/badge/Development%20Stage-alpha-yellowgreen.svg?raw=true "Development Stage") ![Alt text](https://img.shields.io/badge/Test%20Coverage-0%25-lightgrey.svg?raw=true "Test Coverage")

#PCP Friendly Add-ons
`<coded>` by Noah's Light Foundation of Winter Garden, FL, USA

##Purpose
This extension was forked from an internal business process at our charity. Now released to the public, admins and constituents can get a boost during their interaction with PCPs in CiviCRM using this extension.

###Instructions
1. After activating the extension, one new feature will be added to CiviCRM as follows. (More features should be coming)

#####Feature 1: Constituents can see contributions by PCP.
Clicking on the action button "More", a new option appears labeled "Contributions"

![Alt text](./2017NOV13-pcp-contributions.png?raw=true "Feature 1")
