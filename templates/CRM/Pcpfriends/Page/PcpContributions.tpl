{literal}
<style>
    .pcpfriends-pcp-contributions p {
        margin: 0;
        font-size: 13px;
    }
    .cell-note {
        padding: 2px 5px;
        border-radius: 3px;
        text-transform: uppercase;
        background-color: transparent;
        font-size: 90%;
    }
    .pending {
        color: #F48934;
        background-color: #FFE8D5;
    }
    .completed {
        background-color: #eeeeee;
    }
</style>
{/literal}
<div class="pcpfriends-pcp-contributions">
{if $no_contributions}
    <p>No contributions to this PCP</p>
    <!-- insert share buttons -->
{else}
    <table>
        <tr class="columnheader">
            <th>Date Received</th>
            <th>Name</th>
            <th>Message</th>
            <th>Amount</th>
            <th>Status</th>
        </tr>
    {foreach from=$api_results item=contribution}
        <tr>
            <td>{$contribution.receive_date}</td>
            <td>{$contribution.display_name}</td>
            <td>{$contribution.pcp_personal_note}</td>
            <td>{$contribution.amount}</td>
            <td><span class="cell-note {$contribution.contribution_status|@strtolower}">{$contribution.contribution_status}</span></td>
        </tr>
    {/foreach}
    </table>
    <p><span class="cell-note completed">Amount Completed</span> ${$total_amount}</p>
    <p><span class="cell-note pending">Amount Pending</span> ${$pending_amount}</p>
{/if}
</div>