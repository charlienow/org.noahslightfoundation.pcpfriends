<?php
use CRM_Pcpfriends_ExtensionUtil as E;

class CRM_Pcpfriends_Page_PcpContributions extends CRM_Core_Page {

  public function run() {
    CRM_Utils_System::setTitle(E::ts('Contributions'));

    $pcpId = CRM_Utils_Request::retrieve('id', 'Positive', $this, TRUE);

    // api call
    $api_results = civicrm_api3('ContributionSoft', 'get', array(
      'sequential' => 1,
      'pcp_id' => $pcpId,
      'api.Contribution.get' => array(),
    ));
    $notice = empty($api_results['count']) ? TRUE : FALSE;
    $this->assign('no_contributions', $notice);

    if (!$notice):
    // prepare new array to merge the chained api
    $complete_contributions = array();
    $total = 0;
    $pending = 0;

    foreach ($api_results['values'] as &$contribution) {
        $subvalues = $contribution['api.Contribution.get']['values'][0];
        unset($contribution['api.Contribution.get']);

        $single_contribution = array_merge($contribution, $subvalues);

        // sum for total/pending
        if ($single_contribution['contribution_status'] == 'Completed') {
            $total = $total + $single_contribution['amount'];
        } elseif ($single_contribution['contribution_status'] == 'Pending') {
            $pending = $pending + $single_contribution['amount'];
        }

        // format the date
        $old_format = date_create_from_format('Y-m-d H:i:s', $single_contribution['receive_date']);
        $single_contribution['receive_date'] = date_format($old_format, 'M j, Y');

        $complete_contributions[] = $single_contribution;
    }

    $this->assign('api_results', $complete_contributions);
    $this->assign('total_amount', $total);
    $this->assign('pending_amount', $pending);

    endif;

    parent::run();
  }

}
